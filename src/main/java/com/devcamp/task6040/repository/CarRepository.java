package com.devcamp.task6040.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6040.model.Car;

public interface CarRepository extends JpaRepository<Car, Long>  {
    Car findByCarCode(String carCode);
}
